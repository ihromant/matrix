package ua.ihromant;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.concurrent.ThreadLocalRandom;
import java.util.function.LongSupplier;
import java.util.function.Supplier;
import java.util.stream.IntStream;

public class Modulo2SquareMatrixTest {
    private static final int WARMING_COUNT = 10;
    private static final int TASKS_COUNT = 10;
    @Test
    public void testGetSet() {
        for (int sz = 1; sz < 20; sz++) {
            CommonMatrix matr = generateMatrix(sz).modulo(2);
            Modulo2SquareMatrix mod = Modulo2SquareMatrix.from(matr);
            for (int it = 0; it < 15; it++) {
                int i = ThreadLocalRandom.current().nextInt(sz);
                int j = ThreadLocalRandom.current().nextInt(sz);
                mod.set(i, j, mod.get(i, j) + 1);
                matr.set(i, j, (matr.get(i, j) + 1) % 2);
            }
            Assertions.assertEquals(matr, CommonMatrix.from(mod));
        }
    }

    @Test
    public void testMultiply() {
        for (int sz = 1; sz < 40; sz++) {
            CommonMatrix matr1 = generateMatrix(sz).modulo(2);
            CommonMatrix matr2 = generateMatrix(sz).modulo(2);
            Modulo2SquareMatrix mod1 = Modulo2SquareMatrix.from(matr1);
            Modulo2SquareMatrix mod2 = Modulo2SquareMatrix.from(matr2);
            for (Algorithm algo : Algorithm.values()) {
                Assertions.assertEquals(matr1.multiply(matr2).modulo(2),
                        CommonMatrix.from(mod1.multiply(mod2, algo)));
            }
        }
    }

    private long multiplyTime(int size, Algorithm algo) {
        CommonMatrix matr1 = generateMatrix(size).modulo(2);
        CommonMatrix matr2 = generateMatrix(size).modulo(2);
        Modulo2SquareMatrix mod1 = Modulo2SquareMatrix.from(matr1);
        Modulo2SquareMatrix mod2 = Modulo2SquareMatrix.from(matr2);
        return time(() -> mod1.multiply(mod2, algo));
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2, 3})
    public void performanceTest(int idx) {
        int size = 1000;
        Algorithm algo = Algorithm.values()[idx];
        System.out.println("Matrix multiplication with algo " + algo);
        LongSupplier task = () -> multiplyTime(size, algo);
        for (int i = 0; i < WARMING_COUNT; i++) {
            task.getAsLong();
        }
        IntStream.range(0, TASKS_COUNT)
                .mapToLong(i -> task.getAsLong())
                .forEach(time -> System.out.println("It took " + time + "ms to multiply matrices of " + size + " size"));
    }

    private long time(Supplier<?> supp) {
        long start = System.currentTimeMillis();
        Object x = supp.get();
        long result = System.currentTimeMillis() - start;
        System.out.println("Consumed hashcode: " + x.hashCode());
        return result;
    }

    private CommonMatrix generateMatrix(int size) {
        int[][] result = new int[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                result[i][j] = ThreadLocalRandom.current().nextInt(2);
            }
        }
        return CommonMatrix.from(result);
    }
}
