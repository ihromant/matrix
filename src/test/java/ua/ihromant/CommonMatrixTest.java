package ua.ihromant;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CommonMatrixTest {
    @Test
    public void testMultiply() {
        CommonMatrix result = CommonMatrix.from(new int[][]{{1, 2},{3, 4}}).multiply(
                CommonMatrix.from(new int[][]{{5, 6},{7, 8}}));
        Assertions.assertEquals(CommonMatrix.from(new int[][]{{19, 22},{43, 50}}), result);
    }

    @Test
    public void testModulo() {
        CommonMatrix moduled = CommonMatrix.from(new int[][]{{19, 22},{43, 50}}).modulo(2);
        Assertions.assertEquals(CommonMatrix.from(new int[][]{{1, 0},{1, 0}}), moduled);
    }
}
