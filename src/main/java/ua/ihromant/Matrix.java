package ua.ihromant;

public interface Matrix {
    int rows();
    int columns();
    int get(int i, int j);
    void set(int i, int j, int val);
}
