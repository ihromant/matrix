package ua.ihromant;

import java.util.Arrays;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Modulo2SquareMatrix implements Matrix {
    private static final int BLOCK_SIZE = 8;
    private static final long ROW_MASK = shift(BLOCK_SIZE) - 1;
    private final int size;
    private final long[][] blocks;

    public Modulo2SquareMatrix(int size) {
        this.size = size;
        int side = calcSide(size);
        this.blocks = new long[side][side];
    }

    private Modulo2SquareMatrix(int size, long[][] blocks) {
        this.size = size;
        this.blocks = blocks;
    }

    public static Modulo2SquareMatrix from(Matrix matrix) {
        int size = matrix.rows();
        if (size != matrix.columns()) {
            throw new IllegalArgumentException();
        }
        Modulo2SquareMatrix result = new Modulo2SquareMatrix(size);
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                result.set(i, j, matrix.get(i, j));
            }
        }
        return result;
    }

    public static long shift(int i) {
        return 1L << i;
    }

    public Modulo2SquareMatrix multiply(Modulo2SquareMatrix that, Algorithm algo) {
        if (this.size != that.size) {
            throw new IllegalArgumentException();
        }
        switch (algo) {
            case STRASSEN:
                return new Modulo2SquareMatrix(this.size, multiplyStrassen(this.blocks, that.blocks, false));
            case PARALLEL_STRASSEN:
                return new Modulo2SquareMatrix(this.size, multiplyStrassen(this.blocks, that.blocks, true));
            case COMMON:
                return new Modulo2SquareMatrix(this.size, multiplyDirect(this.blocks, that.blocks));
            case COMMON_PARALLEL:
                return new Modulo2SquareMatrix(this.size, multiplyParallelDirect(this.blocks, that.blocks));
            default:
                throw new UnsupportedOperationException();
        }
    }

    @Override
    public int get(int i, int j) {
        if (i < 0 || i >= size || j < 0 || j >= size) {
            throw new IllegalArgumentException();
        }
        int position = (i % BLOCK_SIZE) * BLOCK_SIZE + (j % BLOCK_SIZE);
        return (blocks[i / BLOCK_SIZE][j / BLOCK_SIZE] & shift(position)) == 0 ? 0 : 1;
    }

    @Override
    public void set(int i, int j, int val) {
        if (i < 0 || i >= size || j < 0 || j >= size) {
            throw new IllegalArgumentException();
        }
        int position = (i % BLOCK_SIZE) * BLOCK_SIZE + (j % BLOCK_SIZE);
        if (val % 2 == 0) {
            blocks[i / BLOCK_SIZE][j / BLOCK_SIZE] &= ~shift(position);
        } else {
            blocks[i / BLOCK_SIZE][j / BLOCK_SIZE] |= shift(position);
        }
    }

    @Override
    public int rows() {
        return size;
    }

    @Override
    public int columns() {
        return size;
    }

    private static int calcSide(int size) {
        return Math.max(4, Integer.highestOneBit(size - 1)) / 4;
    }

    private static long add(long a, long b) {
        return a ^ b;
    }

    private static long add(long a, long b, long c, long d) {
        return a ^ b ^ c ^ d;
    }

    private static long[] extractRows(long value) {
        long[] result = new long[BLOCK_SIZE];
        for (int row = 0; row < BLOCK_SIZE; row++) {
            result[row] = extractRow(value, row);
        }
        return result;
    }

    private static long extractRow(long value, int row) {
        return (value & (ROW_MASK << row * BLOCK_SIZE)) >>> row * BLOCK_SIZE;
    }

    private static long extractColumn(long value, int column) {
        long result = 0;
        for (int row = 0; row < BLOCK_SIZE; row++) {
            if ((value & shift(row * BLOCK_SIZE + column)) != 0) {
                result |= shift(row);
            }
        }
        return result;
    }

    private static long multiply(long a, long b) {
        long result = 0;
        long[] rows = extractRows(a);
        for (int j = 0; j < BLOCK_SIZE; j++) {
            long column = extractColumn(b, j);
            for (int i = 0; i < BLOCK_SIZE; i++) {
                if ((Long.bitCount(rows[i] & column) & 1) != 0) {
                    result |= shift(i * BLOCK_SIZE + j);
                }
            }
        }
        return result;
    }

    private static long[][] multiplyDirect(long[][] first, long[][] second) {
        int length = first.length;
        long[][] result = new long[length][length];
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                long val = 0;
                for (int k = 0; k < length; k++) {
                    val = add(val, multiply(first[i][k], second[k][j]));
                }
                result[i][j] = val;
            }
        }
        return result;
    }

    private static long[][] multiplyParallelDirect(long[][] first, long[][] second) {
        int length = first.length;
        long[][] result = new long[length][length];
        IntStream.range(0, length * length)
                .parallel()
                .forEach(t -> {
                    int i = t % length;
                    int j = t / length;
                    long val = 0;
                    for (int k = 0; k < length; k++) {
                        val ^= multiply(first[i][k], second[k][j]);
                    }
                    result[i][j] = val;
                });
        return result;
    }

    private static long[][] multiplyStrassen(long[][] first, long[][] second, boolean parallel) {
        int size = first.length;
        if (size == 1) {
            return new long[][]{{multiply(first[0][0], second[0][0])}};
        }
        if (size == 2) {
            long a11 = first[0][0];
            long a12 = first[0][1];
            long a21 = first[1][0];
            long a22 = first[1][1];
            long b11 = second[0][0];
            long b12 = second[0][1];
            long b21 = second[1][0];
            long b22 = second[1][1];
            long m1 = multiply(add(a11, a22), add(b11, b22));
            long m2 = multiply(add(a21, a22), b11);
            long m3 = multiply(a11, add(b12, b22)); // subtract
            long m4 = multiply(a22, add(b21, b11)); // subtract
            long m5 = multiply(add(a11, a12), b22);
            long m6 = multiply(add(a21, a11), add(b11, b12)); // subtract first
            long m7 = multiply(add(a12, a22), add(b21, b22)); // subtract first
            return new long[][]{
                    {
                            add(m1, m4, m5, m7), // m1 + m4 - m5 + m7
                            add(m3, m5)
                    },
                    {
                            add(m2, m4),
                            add(m1, m2, m3, m6) // m1 - m2 + m3 + m6
                    }};
        } else {
            long[][] a11 = extract(first, 0, 0, size / 2);
            long[][] a12 = extract(first, 0, size / 2, size / 2);
            long[][] a21 = extract(first, size / 2, 0, size / 2);
            long[][] a22 = extract(first, size / 2, size / 2, size / 2);
            long[][] b11 = extract(second, 0, 0, size / 2);
            long[][] b12 = extract(second, 0, size / 2, size / 2);
            long[][] b21 = extract(second, size / 2, 0, size / 2);
            long[][] b22 = extract(second, size / 2, size / 2, size / 2);
            return parallel ? parallelStrassen(size, a11, a12, a21, a22, b11, b12, b21, b22)
                    : sequentialStrassen(size, a11, a12, a21, a22, b11, b12, b21, b22);
        }
    }

    private static long[][] sequentialStrassen(int size, long[][] a11, long[][] a12, long[][] a21, long[][] a22, long[][] b11, long[][] b12, long[][] b21, long[][] b22) {
        long[][] result = new long[size][size];
        long[][] m1 = multiplyStrassen(add(a11, a22), add(b11, b22), false);
        long[][] m2 = multiplyStrassen(add(a21, a22), b11, false);
        long[][] m3 = multiplyStrassen(a11, add(b12, b22), false); // subtract
        long[][] m4 = multiplyStrassen(a22, add(b21, b11), false); // subtract
        long[][] m5 = multiplyStrassen(add(a11, a12), b22, false);
        long[][] m6 = multiplyStrassen(add(a21, a11), add(b11, b12), false); // subtract first
        long[][] m7 = multiplyStrassen(add(a12, a22), add(b21, b22), false); // subtract first
        include(result, add(m1, m4, m5, m7), 0, 0); // m1 + m4 - m5 + m7
        include(result, add(m3, m5), 0, size / 2);
        include(result, add(m2, m4), size / 2, 0);
        include(result, add(m1, m2, m3, m6), size / 2, size / 2); // m1 - m2 + m3 + m6
        return result;
    }

    private static long[][] parallelStrassen(int size, long[][] a11, long[][] a12, long[][] a21, long[][] a22, long[][] b11, long[][] b12, long[][] b21, long[][] b22) {
        long[][] result = new long[size][size];
        long[][][] parts = Stream.<Supplier<long[][]>>of(
                () -> multiplyStrassen(add(a11, a22), add(b11, b22), true),
                () -> multiplyStrassen(add(a21, a22), b11, true),
                () -> multiplyStrassen(a11, add(b12, b22), true),
                () -> multiplyStrassen(a22, add(b21, b11), true),
                () -> multiplyStrassen(add(a11, a12), b22, true),
                () -> multiplyStrassen(add(a21, a11), add(b11, b12), true),
                () -> multiplyStrassen(add(a12, a22), add(b21, b22), true)
        ).parallel().map(Supplier::get).toArray(long[][][]::new);
        include(result, add(parts[0], parts[3], parts[4], parts[6]), 0, 0); // m1 + m4 - m5 + m7
        include(result, add(parts[2], parts[4]), 0, size / 2);
        include(result, add(parts[1], parts[3]), size / 2, 0);
        include(result, add(parts[0], parts[1], parts[2], parts[5]), size / 2, size / 2); // m1 - m2 + m3 + m6
        return result;
    }

    private static long[][] add(long[][] first, long[][] second) {
        long[][] result = new long[first.length][first.length];
        for (int i = 0; i < first.length; i++) {
            for (int j = 0; j < first.length; j++) {
                result[i][j] = add(first[i][j], second[i][j]);
            }
        }
        return result;
    }

    private static long[][] add(long[][] first, long[][] second, long[][] third, long[][] fourth) {
        long[][] result = new long[first.length][first.length];
        for (int i = 0; i < first.length; i++) {
            for (int j = 0; j < first.length; j++) {
                result[i][j] = add(first[i][j], second[i][j], third[i][j], fourth[i][j]);
            }
        }
        return result;
    }

    private static void include(long[][] target, long[][] part, int row, int column) {
        for (int i = 0; i < part.length; i++) {
            System.arraycopy(part[i], 0, target[row + i], column, part.length);
        }
    }

    private static long[][] extract(long[][] from, int row, int column, int size) {
        long[][] result = new long[size][size];
        for (int i = 0; i < size; i++) {
            System.arraycopy(from[row + i], column, result[i], 0, size);
        }
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Modulo2SquareMatrix that = (Modulo2SquareMatrix) o;
        return size == that.size && Arrays.deepEquals(blocks, that.blocks);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(size);
        result = 31 * result + Arrays.deepHashCode(blocks);
        return result;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < rows(); i++) {
            builder.append('(');
            for (int j = 0; j < columns() - 1; i++) {
                builder.append(get(i, j));
                builder.append(',');
            }
            builder.append(get(i, columns() - 1));
            builder.append(')');
            builder.append('\n');
        }
        return builder.toString();
    }
}
