package ua.ihromant;

import java.util.Arrays;
import java.util.stream.Collectors;

public class CommonMatrix implements Matrix {
    private final int[][] numbers;

    public CommonMatrix(int rows, int columns) {
        this.numbers = new int[rows][columns];
    }

    private CommonMatrix(int[][] numbers) {
        this.numbers = Arrays.stream(numbers).map(int[]::clone).toArray(int[][]::new);
    }

    public static CommonMatrix from(int[][] from) {
        return new CommonMatrix(from);
    }

    public static CommonMatrix from(Matrix other) {
        CommonMatrix result = new CommonMatrix(other.rows(), other.columns());
        for (int i = 0; i < other.rows(); i++) {
            for (int j = 0; j < other.columns(); j++) {
                result.set(i, j, other.get(i, j));
            }
        }
        return result;
    }

    @Override
    public int get(int i, int j) {
        if (i < 0 || i >= rows() || j < 0 || j >= columns()) {
            throw new IllegalArgumentException();
        }
        return numbers[i][j];
    }

    @Override
    public void set(int i, int j, int val) {
        if (i < 0 || i >= rows() || j < 0 || j >= columns()) {
            throw new IllegalArgumentException();
        }
        numbers[i][j] = val;
    }

    @Override
    public int rows() {
        return numbers.length;
    }

    @Override
    public int columns() {
        return numbers[0].length;
    }

    public CommonMatrix modulo(int val) {
        if (val < 2) {
            throw new IllegalArgumentException();
        }
        CommonMatrix result = new CommonMatrix(numbers);
        for (int i = 0; i < result.rows(); i++) {
            for (int j = 0; j < result.columns(); j++) {
                result.numbers[i][j] = Math.floorMod(result.numbers[i][j], val);
            }
        }
        return result;
    }

    public CommonMatrix multiply(CommonMatrix that) {
        int length = this.columns();
        if (length != that.rows()) {
            throw new IllegalArgumentException("First columns different from second rows");
        }
        int rows = this.rows();
        int columns = that.columns();
        CommonMatrix result = new CommonMatrix(rows, columns);
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                int val = 0;
                for (int k = 0; k < length; k++) {
                    val += this.get(i, k) * that.get(k, j);
                }
                result.set(i, j, val);
            }
        }
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommonMatrix matrix = (CommonMatrix) o;
        return Arrays.deepEquals(numbers, matrix.numbers);
    }

    @Override
    public int hashCode() {
        return Arrays.deepHashCode(numbers);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < rows(); i++) {
            builder.append('(');
            builder.append(Arrays.stream(numbers[i]).mapToObj(String::valueOf).collect(Collectors.joining(",")));
            builder.append(')');
            builder.append('\n');
        }
        return builder.toString();
    }
}
