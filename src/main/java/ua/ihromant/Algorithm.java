package ua.ihromant;

public enum Algorithm {
    STRASSEN, PARALLEL_STRASSEN, COMMON, COMMON_PARALLEL
}
